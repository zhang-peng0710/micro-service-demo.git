# MicroServiceDemo

#### 介绍
自己搭建的微服务demo,都运行在本机,代码中使用了cap，如果没安装MQ可以把startup中的的cap方法注释掉

#### 软件架构
使用Consul作为服务注册组件,Ocelot作为网关,IdentityServer4作为授权中心,其中WebClient为客户端,用的授权码模式,请访问WebClien的https://localhost:7058进行授权

#### 使用说明

1.  在项目根目录直接运行run.bat文件,文件中用cmd来启动各个服务,也可自行修改cmd命令

