echo "start consul"
start cmd /k "title consul&&cd consul_1.15.1_windows_386&&consul agent -dev"
echo "started consul"

echo "start APIGateway,port:7240"
start cmd /k "title Gateway&&cd MicroServiceDemo\Ocelot&&dotnet run --urls=http://localhost:7240"
echo "started APIGateway"

echo "start IdentityServer,port:7878"
start cmd /k "title IdentityServer&&cd  MicroServiceDemo\Id4&&dotnet run --urls=https://localhost:7878"
echo "started APIGateway"

echo "start IdentityClient,port:7058"
start cmd /k "title IdentityClient&&cd MicroServiceDemo\WebClient&&dotnet run --urls=https://localhost:7058"
echo "started APIGateway"


echo  "start productservice,port:8848,8849"
start cmd /k "title pro8848&&cd  MicroServiceDemo\ProductService&&dotnet run --urls=http://localhost:8848 --serviceport=8848  serviceName=productservice"
start cmd /k "title pro8849&&cd  MicroServiceDemo\ProductService&&dotnet run --urls=http://localhost:8849 --serviceport=8849  serviceName=orderservice"
echo "productservice started——————"


echo  "start orderservice,port:9000,9001"
start cmd /k "title order9000&&cd  MicroServiceDemo\OrderService&&dotnet run --urls=http://localhost:9000 --serviceport=9000  serviceName=orderservice"
start cmd /k "title order9001&&cd  MicroServiceDemo\OrderService&&dotnet run --urls=http://localhost:9001 --serviceport=9001  serviceName=orderservice"
echo "orderservice started——————"


