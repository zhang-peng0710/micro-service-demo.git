﻿using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using System.IO.Pipelines;
using static GrpcClient.Greeter;

namespace GrpcClient.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly Greeter.GreeterClient _greeterClient;
        public HomeController(Greeter.GreeterClient greeterClient)
        {
            _greeterClient = greeterClient;
        }

        public async Task<IActionResult> TestGrpcStream()
        {
            //var pipe=HttpContext.Request.BodyReader;
            //ReadResult result = await pipe.ReadAsync();
            //result.
            var metadata = new Metadata();
            metadata.Add("test_header", "test");
            var replay=_greeterClient.TestStream(metadata);
            var readTask = Task.Run(async () => 
            {
                await foreach (var stream in replay.ResponseStream.ReadAllAsync())
                {
                    Console.WriteLine("服务端返回:"+stream.Message);
                }
            });

            while (true)
            {
                if (DateTime.Now.Minute == 30)
                    break;
                await replay.RequestStream.WriteAsync(new HelloRequest() { Name = "我叫张" + Random.Shared.Next(1, 10) });
                await Task.Delay(1000);
            }
                
            await replay.RequestStream.CompleteAsync();
            await readTask;
            return Ok();
        }
    }
}
