﻿using Microsoft.EntityFrameworkCore;
using OrderService.Models;

namespace OrderService.DbContexts
{
    public class OrderContext:DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options)
           : base(options)
        {
        }

        public DbSet<Order> orders { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().ToTable("Order");

        }
    }
}
