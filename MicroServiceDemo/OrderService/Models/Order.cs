﻿namespace OrderService.Models
{
    public class Order
    {
        public int Id { get; set; }

        public string OrderNo { get; set; }
    }
}
