using IdentityServer4.AccessTokenValidation;
using Ocelot.Cache.CacheManager;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Ocelot.Provider.Polly;
using Ocelot.Values;
using System.Net;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Configuration.AddJsonFile("OcelotConfig.json",false,true);
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
        .AddIdentityServerAuthentication("orderService", options =>
        {
            options.Authority = "https://localhost:7878";//鉴权中心地址
            options.ApiName = "orderAPI";
            options.SupportedTokens = SupportedTokens.Both;
            //options.ApiSecret = "orderApi secret";
            options.RequireHttpsMetadata = false;
        })
        .AddIdentityServerAuthentication("productService", options =>
        {
            options.Authority = "https://localhost:7878";//鉴权中心地址
            options.ApiName = "productAPI";
            options.SupportedTokens = SupportedTokens.Both;
            //options.ApiSecret = "productApi secret";
            options.RequireHttpsMetadata = false;
        });
builder.Services
    .AddOcelot()
    .AddConsul()
    .AddPolly()
    .AddCacheManager(config=>config.WithDictionaryHandle());

var app = builder.Build();
//var lifetime= app.Services.GetRequiredService<IHostApplicationLifetime>();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseOcelot().Wait();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
