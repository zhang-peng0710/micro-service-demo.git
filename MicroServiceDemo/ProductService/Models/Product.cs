﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProductService.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public bool Enable { get; set; }

        public decimal Price { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
