﻿using Consul;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductService.Consul
{
    public  class ConsulServiceHelper
    {
        private static ConcurrentDictionary<string,string> _productServices = new ConcurrentDictionary<string,string>();
        private static ConcurrentDictionary<string, string> _ordertServices = new ConcurrentDictionary<string, string>();
        private readonly ConsulClient _consulClient;
        private readonly IConfiguration _configuration;
        public ConsulServiceHelper(IConfiguration configuration)
        {
            _configuration= configuration;
            _consulClient =new ConsulClient(consul =>
            {
                consul.Address = new Uri(_configuration.GetSection("consul")["Address"]);
               
            });
        }

        public void GetServices()
        {
            var serviceNames = new string[] { "productservice" };
            Array.ForEach(serviceNames, name =>
            {
                Task.Run(() =>
                {
                    //WaitTime默认为5分钟
                    var queryOptions = new QueryOptions { WaitTime = TimeSpan.FromMinutes(10) };
                    while (true)
                    {
                        var res = _consulClient.Health.Service(name, null, true, queryOptions).Result;

                        //控制台打印一下获取服务列表的响应时间等信息
                        Console.WriteLine($"{DateTime.Now}获取{name}：queryOptions.WaitIndex：{queryOptions.WaitIndex}  LastIndex：{res.LastIndex}");

                        //版本号不一致 说明服务列表发生了变化
                        if (queryOptions.WaitIndex != res.LastIndex)
                        {
                            queryOptions.WaitIndex = res.LastIndex;

                            //服务地址列表
                            var serviceUrls = res.Response.Select(p => $"{p.Service.Address + ":" + p.Service.Port}").ToList();
                            serviceUrls.ForEach(p => { Console.WriteLine("consul健康服务:"+p); });
                            ChangeServices(serviceUrls, name);
                        }
                    }
                });
            });
           
        }

        /// <summary>
        /// 变更服务数量
        /// </summary>
        /// <param name="services"></param>
        /// <param name="serviceName"></param>
        private static void ChangeServices(List<string> services,string serviceName)
        {
            if (serviceName == "orderservice")
            {
                _ordertServices.Clear();
                services.ForEach(a =>
                {
                   
                    _ordertServices.TryAdd(serviceName + ":" + a, a);
                    
                });
            }
            else if (serviceName == "productservice")
            {
                _productServices.Clear();
                services.ForEach(a =>
                {
                    _productServices.TryAdd(serviceName + ":" + a, a);
                });
            }
           
        }

        /// <summary>
        /// 随机获取一个服务的真实url
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public string GetRandomServiceUrl(string serviceName)
        {
            string serviceUrl=string.Empty;
            if (serviceName=="productservice")
            {
                Console.WriteLine(_productServices.Keys.ToString());
                serviceUrl = _productServices.ElementAtOrDefault(Random.Shared.Next(0, _productServices.Count)).Value;
            }
            else if(serviceName == "orderservice")
            {
                serviceUrl = _ordertServices.ElementAtOrDefault(Random.Shared.Next(0, _ordertServices.Count)).Value;
            }
            return serviceUrl;
        }
    }
}
