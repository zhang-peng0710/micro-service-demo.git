﻿using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using static System.Net.WebRequestMethods;

namespace ProductService.Consul
{
    public static class ConsulHelper
    {
        public static IApplicationBuilder RegisterConsul(
            this IApplicationBuilder applicationBuilder
            ,IHostApplicationLifetime hostApplicationLifetime
            ,IConfiguration configuration)
        {
            ConsulClient consulClient = new ConsulClient(consul => 
            {
                consul.Address =new Uri(configuration.GetSection("consul")["Address"]);
                consul.Datacenter = configuration.GetSection("consul")["Datacenter"];
            });
            //consulClient.Agent.ServiceDeregister("cdf7f896-7f1b-48e7-ad6d-a21d447d3cef");
            Dictionary<string,string> serviceMetaData = new Dictionary<string,string>();//服务元数据
            serviceMetaData.Add("zp", "zhangpeng");
            string serviceAddr = configuration.GetSection("consul")["ServiceAddr"];//consul服务地址
            AgentServiceRegistration agentServiceRegistration=new AgentServiceRegistration();
            agentServiceRegistration.ID=Guid.NewGuid().ToString();
            agentServiceRegistration.Address = configuration.GetSection("consul")["ServiceAddr"];//自定义服务的地址，直接写ip，不用加http，不然ocelot一直报错
            agentServiceRegistration.Name = configuration["serviceName"] is null? "productservice": configuration["serviceName"];//这里读取命令行设置的名称
            agentServiceRegistration.Port = configuration["serviceport"] is null? 5000:int.Parse(configuration["serviceport"]);//这里读取命令行设置的自定义服务的端口
            agentServiceRegistration.Meta = serviceMetaData;
            agentServiceRegistration.Check = new AgentServiceCheck()
            {
                HTTP = $"http://{serviceAddr}:{agentServiceRegistration.Port}{configuration.GetSection("consul")["ServiceHealthCheck"]}",//健康检查地址
                Method = "GET",//请求方式
                Timeout = TimeSpan.FromSeconds(5),//超时时间
                Interval = TimeSpan.FromSeconds(10),//间隔时间
                DeregisterCriticalServiceAfter= TimeSpan.FromSeconds(30)//多少秒后连不上则从consul中清除该服务

            };
            //Console.WriteLine(agentServiceRegistration.Check.HTTP);
            consulClient.Agent.ServiceRegister(agentServiceRegistration).Wait();

            //监听应用程序终止回调,把服务从consul移除
            hostApplicationLifetime.ApplicationStopping.Register(() => 
            {
                consulClient.Agent.ServiceDeregister(agentServiceRegistration.ID).Wait();
            });
            hostApplicationLifetime.ApplicationStopped.Register(() =>
            {
                consulClient.Agent.ServiceDeregister(agentServiceRegistration.ID).Wait(); ;
            });
            return applicationBuilder;
        }
    }
}
