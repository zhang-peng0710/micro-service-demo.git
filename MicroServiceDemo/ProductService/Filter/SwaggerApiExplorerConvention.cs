﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace ProductService.Filter
{
    public class SwaggerApiExplorerConvention : IActionModelConvention
    {
        /// <summary>
        /// 忽略某个action
        /// </summary>
        /// <param name="action"></param>
        public void Apply(ActionModel action)
        {
            if (action.Attributes.Count == 0)
                action.ApiExplorer.IsVisible = false;

        }
    }
}
