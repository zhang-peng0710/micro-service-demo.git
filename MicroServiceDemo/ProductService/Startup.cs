using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Nacos.AspNetCore.V2;
using ProductService.Consul;
using ProductService.DbContexts;
using ProductService.Elastic;
using ProductService.Filter;
using ProductService.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ProductService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddNacosAspNet(Configuration, "nacos");
            services.AddSingleton<ConsulServiceHelper>();
            services.AddControllers(option=>option.Conventions.Add(new SwaggerApiExplorerConvention()));
            services.AddSingleton<ElasticClientHelper>();
            services.Configure<TestOption>(Configuration.GetSection("TestOption"));
            services.AddHttpClient();
            var connStr = Configuration.GetConnectionString("DefaultConnStr");
            services.AddDbContext<ProductContext>(option=>option.UseMySql(connStr,ServerVersion.AutoDetect(connStr)));
            services.AddCap(setup=>
            {
                setup.UseEntityFramework<ProductContext>();
                setup.UseRabbitMQ(rabbit => 
                {
                    rabbit.HostName = "localhost";
                });
            });
            

            services.AddSwaggerGen(c =>
            {
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProductService", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,IHostApplicationLifetime hostApplicationLifetime, ConsulServiceHelper consulServiceHelper)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProductService v1"));
            }
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.RegisterConsul(hostApplicationLifetime, Configuration);
            //ConsulServiceHelper consulServiceHelper= app.ApplicationServices.GetService(typeof(ConsulServiceHelper)) as ConsulServiceHelper;
            //consulServiceHelper.GetServices();
            
        }
    }
}
