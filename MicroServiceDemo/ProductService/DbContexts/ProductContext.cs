﻿using Microsoft.EntityFrameworkCore;
using ProductService.Models;

namespace ProductService.DbContexts
{
    public class ProductContext:DbContext
    {
       
        public ProductContext(DbContextOptions<ProductContext> options)
           : base(options)
        {
        }

        public DbSet<Product> products { get; set; }
       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Product");
          
        }
    }
}
