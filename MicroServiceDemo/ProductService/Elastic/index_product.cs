﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProductService.Elastic
{
    public class index_product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public bool Enable { get; set; }

        public decimal Price { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
