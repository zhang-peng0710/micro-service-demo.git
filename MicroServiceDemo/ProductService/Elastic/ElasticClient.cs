﻿
using Microsoft.Extensions.Configuration;
using Nest;
using System;

namespace ProductService.Elastic
{
    public class ElasticClientHelper
    {
        private readonly IConfiguration _configuration;
        private readonly ElasticClient _elasticClient;
        public ElasticClientHelper(IConfiguration configuration)
        {
            _configuration=configuration;
            _elasticClient = InitElasticClient();
        }
        private ElasticClient InitElasticClient()
        {
            var uri = new Uri("https://localhost:9200");
            var settings = new ConnectionSettings(uri)
                .CertificateFingerprint("656e023e9a394c670e0b056d55ad12c2b8d0bf6e9b2a7373fbd38fa711f2d460")
                .BasicAuthentication(_configuration.GetSection("EsConfig")["UserName"], _configuration.GetSection("EsConfig")["Pwd"]);

            var client = new ElasticClient(settings);
            return client;
        }
       
        public ElasticClient GetElasticClient()
        {
            return _elasticClient;
        }
    }
}
