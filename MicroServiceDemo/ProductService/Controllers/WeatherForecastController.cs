﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Consul;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using ProductService.Consul;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using ProductService.Options;
using DotNetCore.CAP.Messages;
using Google.Protobuf.WellKnownTypes;
using System.Net;
using Microsoft.VisualBasic.FileIO;
using DotNetCore.CAP;
using ProductService.DbContexts;
using Microsoft.EntityFrameworkCore;
using ProductService.Models;
using ProductService.Elastic;
using Nest;

namespace ProductService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ConsulServiceHelper _consulServiceHelper;
        private readonly IOptions<TestOption> _options;
        private readonly IOptionsMonitor<TestOption> _optionsMonitor;
        private readonly IOptionsSnapshot<TestOption> _optionsSnapshot;
        private readonly ICapPublisher _capBus;
        private readonly ProductContext _productContext;
        private readonly ElasticClientHelper _elasticClientHelper;
        public WeatherForecastController(
            ILogger<WeatherForecastController> logger
            , IConfiguration configuration
            , IHttpClientFactory httpClientFactory
            , ConsulServiceHelper consulServiceHelper
            , IOptions<TestOption> options
            , IOptionsMonitor<TestOption> optionsMonitor
            , IOptionsSnapshot<TestOption> optionsSnapshot
            , ICapPublisher capPublisher
            , ProductContext productContext
            , ElasticClientHelper elasticClientHelper
            )
        {
            _logger = logger;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            _consulServiceHelper = consulServiceHelper;
            _options = options;
            _optionsMonitor = optionsMonitor;
            _optionsSnapshot = optionsSnapshot;
            _capBus = capPublisher;
            _productContext = productContext;
            _elasticClientHelper = elasticClientHelper;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var ss = _options.Value.secret;
            var bb = _optionsMonitor.CurrentValue.secret;
            var cc = _optionsSnapshot.Value.secret;
            var configValue = _configuration.GetSection("TestOption")["secret"];
            Console.WriteLine("读取option:" + ss);
            Console.WriteLine("读取_optionsMonitor:" + bb);
            Console.WriteLine("读取_optionsSnapshot:" + cc);
            Console.WriteLine("读取configValue:" + configValue);
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("/HealthCheck")]
        public string HealthCheck()
        {
            return "HealthCheck";
        }

        [HttpGet("/ConsulTest")]
        public async Task<string> ConsulTest()
        {
            var consulClient = new ConsulClient(consulClient =>
            {
                consulClient.Address = new Uri(_configuration.GetSection("consul")["Address"]);
            });

            var services = consulClient.Health.Service("productservice").Result.Response;
            var serviceUrl = services.Select(a => new { a.Service.Address, a.Service.Port }).ElementAt(Random.Shared.Next(0, services.Length));
            var client = _httpClientFactory.CreateClient();
            var result = await client.GetStringAsync(serviceUrl.Address + ":" + serviceUrl.Port + "/GetProductList");
            return $"访问地址：{serviceUrl.Address}:{serviceUrl.Port},返回数据{result}";
        }

        [HttpGet("/ConsulTest2")]
        public async Task<string> ConsulTest2(string serviceName = "productservice")
        {
            string serviceUrl = _consulServiceHelper.GetRandomServiceUrl(serviceName);
            var client = _httpClientFactory.CreateClient();
            var result = await client.GetStringAsync(serviceUrl + "/GetProductList");
            return $"访问地址：{serviceUrl},返回数据{result}";
        }

        [HttpGet("/GetProductList")]
        public IEnumerable<string> GetProductList()
        {
            return new List<string>() { "苹果" + Random.Shared.Next(0, 100), "桃子" + Random.Shared.Next(0, 100), "香蕉" + Random.Shared.Next(0, 100) };
        }

        [HttpGet("/GetServiceInfo")]
        public string GetServiceInfo()
        {
            string addr = HttpContext.Connection.LocalIpAddress.ToString();
            string port = HttpContext.Connection.LocalPort.ToString();
            return $"服务地址{addr}:{port},{DateTime.Now}请求了GetServiceInfo";
        }

        [HttpGet("/TestQos")]
        public IActionResult TestQos()
        {
            string addr = HttpContext.Connection.LocalIpAddress.ToString();
            string port = HttpContext.Connection.LocalPort.ToString();
            throw new Exception($"服务地址{addr}:{port},{DateTime.Now}请求了TestQos");
            //return BadRequest($"服务地址{addr}:{port},{DateTime.Now}请求了TestQos");
        }

        [NonAction]
        [CapSubscribe("orderservice.create.order")] 
        public async Task TestCap(string orderNo)
        {
            Console.WriteLine("接收cap数据:"+orderNo);
            var product= _productContext.products.FirstOrDefault(a => a.Id == 1);
            product.Count= product.Count-1;
            Console.WriteLine($"当前库存:{product.Count}");
            _productContext.SaveChanges();
            await Task.CompletedTask;
        }

       
    }
}
