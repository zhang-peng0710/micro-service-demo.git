﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProductService.DbContexts;
using ProductService.Elastic;
using ProductService.Models;
using ProductService.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProductService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EsController : ControllerBase
    {
       
        private readonly ElasticClientHelper _elasticClientHelper;
        private readonly ProductContext _productContext;
        public EsController(
             ElasticClientHelper elasticClientHelper
            , ProductContext productContext
            )
        {
            _elasticClientHelper = elasticClientHelper;
            _productContext=productContext;
        }
        /// <summary>
        /// 获取索引
        /// </summary>
        /// <returns></returns>
        [HttpGet("/GetIndices")]
        public async Task<IActionResult> GetIndices()
        {
            var esClient = _elasticClientHelper.GetElasticClient();
            var indices = await esClient.Cat.IndicesAsync();
            return Ok(indices.Records);
        }
       
        /// <summary>
        /// 创建索引
        /// </summary>
        /// <returns></returns>
        [HttpGet("/CreateIndex")]
        public async Task<IActionResult> CreateIndex()
        {
            var esClient = _elasticClientHelper.GetElasticClient();
            var response = await esClient.Indices
                .CreateAsync("index_product", option => option.Map(
                    map => map.AutoMap<index_product>()));

            return Ok(response);
        }


        /// <summary>
        /// 新增文档
        /// </summary>
        /// <returns></returns>
        [HttpGet("/InsertDoc")]
        public async Task<IActionResult> InsertDoc()
        {
            var esClient = _elasticClientHelper.GetElasticClient();

            Product product = new Product() { Count = 999, CreateTime = DateTime.Now, Enable = true, Name = "测试商品", Price = 65.05M };
            await _productContext.products.AddAsync(product);
            var result =await _productContext.SaveChangesAsync();
            if (result > 0)
            {
                var response = await esClient.IndexAsync(product, p => p.Index("index_product").Id(product.Id));
                return Ok(response);
            }
            else
            {
                return Ok("插入数据库失败");
            }
        }
    }
}
