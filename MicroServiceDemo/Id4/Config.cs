﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;

namespace Id4
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("orderScope"),
                new ApiScope("productScope"),
            };

        public static IEnumerable<ApiResource> ApiResources =>
       new ApiResource[]
       {
            new ApiResource("orderAPI","订单API")
            {
                //!!!重要
                Scopes = { "orderScope" }
            },
           new ApiResource("productAPI","产品API")
           {
               //!!!重要
               Scopes = { "productScope" }
           }
       };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // interactive client using code flow + pkce
                new Client
                {
                    ClientId = "webclient",
                    ClientSecrets = { new Secret("webclient".Sha256()) },
                    ClientName = "webclient",
                    AllowedGrantTypes = GrantTypes.Code,

                    RedirectUris = { "https://localhost:7058/signin-oidc" },
                    FrontChannelLogoutUri = "https://localhost:7058/signout-oidc",
                    PostLogoutRedirectUris = { "https://localhost:7058/signout-callback-oidc" },

                    //RedirectUris = {"https://localhost:7058/signin-oidc" },
                    //FrontChannelLogoutUri = "http://localhost:7058/home/index.html",
                    //PostLogoutRedirectUris = { "http://localhost:7058/home/index.html" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "productScope", "orderScope" },
                    RequireConsent = true,//是否显示同意界面
                    AllowRememberConsent = false,//是否记住同意选项
                },
            };
    }
}