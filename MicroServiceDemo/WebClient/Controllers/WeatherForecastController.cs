using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebClient.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        public WeatherForecastController(ILogger<WeatherForecastController> logger
            , IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory= httpClientFactory;
        }

        
        [HttpGet(Name = "GetWeatherForecast")]
        public async Task<IActionResult> Get()
        {
            var access_token=await HttpContext.GetTokenAsync("access_token");
            var client=_httpClientFactory.CreateClient();
            var gateway = "http://localhost:7240";
            var downStreamUrl = "/product/GetProductList";
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {access_token}");
            return Ok(await client.GetStringAsync(gateway + downStreamUrl));
        }
    }
}